FROM hotio/base@sha256:453a0df72a0639063c43d39bfa60063ef0657a99b2fe8f4ce55d20b2ad353bd7

ARG DEBIAN_FRONTEND="noninteractive"

ENV DEEMIX_DATA_DIR="/config/app"
ENV DEEMIX_MUSIC_DIR="/downloads"

EXPOSE 6595

# install packages
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash && \
    apt install -y --no-install-recommends --no-install-suggests \
        nodejs netbase opus-tools && \
    npm install --global yarn && \
# clean up
    apt autoremove -y && \
    apt clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

ARG VERSION
ARG PACKAGE_VERSION=${VERSION}
RUN mkdir -p "${APP_DIR}/build/deemix" && \
    curl -fsSL "https://git.freezerapp.xyz/kermit/deemix-js/archive/sync-executecommand.tar.gz" | tar xzf - -C "${APP_DIR}/build/deemix" --strip-components=1 && \
    mkdir -p "${APP_DIR}/build/gui" && \
    curl -fsSL "https://gitlab.com/RemixDev/deemix-gui/-/archive/main/deemix-gui.tar.gz" | tar xzf - -C "${APP_DIR}/build/gui" --strip-components=1 && \
    curl -fsSL "https://gitlab.com/RemixDev/deemix-webui/-/archive/main/deemix-webui-main.tar.gz" | tar xzf - -C "${APP_DIR}/build/gui/webui" --strip-components=1 && \
    rm -rf "${APP_DIR}/build/gui/server/dist" && \
    yarn --cwd "${APP_DIR}/build/deemix" install --frozen-lockfile && \
    yarn --cwd "${APP_DIR}/build/deemix" link && \
    yarn --cwd "${APP_DIR}/build/gui/server" link deemix && \
    yarn --cwd "${APP_DIR}/build/gui/server" install --frozen-lockfile && \
    yarn --cwd "${APP_DIR}/build/gui/server" build && \
    yarn --cwd "${APP_DIR}/build/gui/webui" install --frozen-lockfile && \
    yarn --cwd "${APP_DIR}/build/gui/webui" build && \
    mkdir -p "${APP_DIR}/bin/server" && \
    cp -r "${APP_DIR}/build/gui/package.json" "${APP_DIR}/bin/" && \
    cp -r "${APP_DIR}/build/gui/server/dist" "${APP_DIR}/bin/server" && \
    mkdir "${APP_DIR}/bin/webui" && \
    cp -r "${APP_DIR}/build/gui/webui/public" "${APP_DIR}/bin/webui" && \
    rm -rf "${APP_DIR}/build" && \
    chmod -R u=rwX,go=rX "${APP_DIR}" && \
    mkdir -p "${DEEMIX_MUSIC_DIR}" && \
    chmod -R u=rwX,go=rX "${DEEMIX_MUSIC_DIR}"

COPY root/ /
